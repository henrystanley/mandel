
all: mandle

mandle: main.o lodepng.o
	cc main.o lodepng.o -o mandle

main.o: main.c
	cc -O3 -c main.c

lodepng.o: lodepng.c
	cc -O3 -c lodepng.c

clean:
	rm *.o mandle
