#include "lodepng.h"

#include <stdio.h>
#include <stdlib.h>

////////////////
// Parameters //
////////////////

#define MAX_ITER 200


///////////////////////////////////
// Complex Number Implementation //
///////////////////////////////////

typedef struct ComplexN {
  double r;
  double i;
} ComplexN;

ComplexN addComplex(ComplexN a, ComplexN b) {
  ComplexN c;
  c.r = a.r + b.r;
  c.i = a.i + b.i;
  return c;
}

ComplexN mulComplex(ComplexN a, ComplexN b) {
  ComplexN c;
  c.r = (a.r * b.r) - (a.i * b.i);
  c.i = (a.i * b.r) + (a.r * b.i);
  return c;
}


////////////////////////////////
// Mandelbrot Set Calculation //
////////////////////////////////

/* type used to store iteration count */
typedef unsigned char MandelVal;

/* returns the number of iterations necessary to
   determine if a point is a member of the mandelbrot set */
MandelVal mandel(double x, double y) {
  MandelVal iter = 0;
  ComplexN c;
  c.r = x;
  c.i = y;
  ComplexN z = c;
  while (iter < MAX_ITER && (z.r*z.r + z.i*z.i) < 4) {
  	z = addComplex(c, mulComplex(z, z));
  	iter++;
  }
  return iter;
}

/* type used to store a two dimentional array of mandel vals */
typedef struct MandelField {
  MandelVal *field;
  int sizeX;
  int sizeY;
} MandelField;

/* generates a MandelField for a given area */
MandelField mandelAt(double x, double y, double delta, int stepNX, int stepNY) {

  /* allocate new MandleField */
  MandelField mf;
  mf.field = (MandelVal *) malloc(stepNY * stepNX * sizeof(MandelVal));
  mf.sizeX = stepNX;
  mf.sizeY = stepNY;

  /* make sure we had enough space for the field */
	if (mf.field == NULL) {
		fprintf(stderr, "out of memory\n");
		exit(-1);
	}

  /* calculate mandel vals for field */
  double mX, mY;
  int stepX, stepY;
  mY = y;
  for (stepY = 0; stepY < stepNY; stepY++) {
    mX = x;
    for (stepX = 0; stepX < stepNX; stepX++) {
      mf.field[(stepY * stepNX) + stepX] = mandel(mX, mY);
      mX += delta;
    }
    mY += delta;
  }
  return mf;
}


//////////////////////////////
// Mandelbrot Set Rendering //
//////////////////////////////

void renderMandelField(MandelField mf, char* png_name) {
  /* allocate space for image array */
  unsigned char *image;
  image = (unsigned char *) malloc(mf.sizeX * mf.sizeY * sizeof(unsigned char) * 4);

  /* make sure we had enough space for the image array */
	if (image == NULL) {
		fprintf(stderr, "out of memory\n");
		exit(-1);
	}

  /* get image RGBA data from field */
  int x, y;
  for (y = 0; y < mf.sizeY; y++) {
    for (x = 0; x < mf.sizeX; x++) {
      int fieldIndex = ((y * mf.sizeX) + x);
      int pixelIndex = fieldIndex * 4;
      unsigned char pixelVal = (unsigned char)(((float)mf.field[fieldIndex] / MAX_ITER) * 255);
      image[pixelIndex] = pixelVal;
      image[pixelIndex+1] = pixelVal;
      image[pixelIndex+2] = pixelVal;
      image[pixelIndex+3] = 255;
    }
  }


  /* save mandel field image as png */
  unsigned err = lodepng_encode32_file(png_name, image, mf.sizeX, mf.sizeY);

  /* print error in case something went wrong */
  if(err) {
    printf("error with png export %u: %s\n", err, lodepng_error_text(err));
  }

  /* free the memory for our image array */
  free(image);
}


//////////
// Main //
//////////

int main (int argc, char *argv[]) {

  /* get input parameters */
  if (argc != 6) {
    fprintf(stderr, "wrong number of args\n");
    exit(-1);
  }
  double paramX1 = strtod(argv[1], NULL);
  double paramY1 = strtod(argv[2], NULL);
  double paramX2 = strtod(argv[3], NULL);
  double paramY2 = strtod(argv[4], NULL);
  int paramResX = atoi(argv[5]);

  /* calculate render parameters */
  double paramDelta = (paramX2 - paramX1) / paramResX;
  int paramResY = (paramY2 - paramY1) / paramDelta;

  /* calculate mandelbrot set */
  MandelField m = mandelAt(paramX1, paramY1, paramDelta, paramResX, paramResY);

  /* render mandelbrot set */
  renderMandelField(m, "mandle.png");

  return 0;
}
