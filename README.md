
# Mandle #

This is a small program written in C that renders a portion of the
mandelbrot set as a PNG.


## Building ##

Building should be as simple as typing:

    $ make

Hopefully this should work

## Running ##

The program must be provided with 5 parameters:

1. x position of the bottom left corner
2. y position of the bottom left corner
3. x position of the upper right corner
4. y position of the upper right corner
5. x resolution

Here's an example:

    ./mandle -2 -1.3 1 1.3 2000


## Thanks ##

This program utilizes the fabulous PNG library, Lodepng.
Much thanks goes to its developer, Lode Vandevenne.
